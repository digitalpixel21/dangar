import 'package:dangar/constants/color_constant.dart';
import 'package:dangar/screens/quiz/quiz_screen.dart';
import 'package:dangar/screens/tab/account_screen.dart';
import 'package:dangar/screens/tab/home_screen.dart';
import 'package:flutter/material.dart';

class BottomTabInitial extends StatefulWidget {
  const BottomTabInitial({Key? key}) : super(key: key);

  @override
  State<BottomTabInitial> createState() => _BottomTabInitialState();
}

/// This is the private State class that goes with BottomTabInitial.
class _BottomTabInitialState extends State<BottomTabInitial> {
  int _selectedIndex = 0;
  final screen = [
    const HomeScreen(),
    const QuizzScreen(),
    const AccountScreen()
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: screen[_selectedIndex],
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home_filled),
            label: 'Beranda',
            backgroundColor: Colors.blueAccent,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.task),
            label: 'Kuis',
            backgroundColor: Colors.blueAccent,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.manage_accounts_sharp),
            label: 'Akun',
            backgroundColor: Colors.lightBlueAccent,
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: mBlueColor,
        unselectedItemColor: mGreyColor,
        onTap: _onItemTapped,
        backgroundColor: Colors.transparent,
        type: BottomNavigationBarType.fixed,
        selectedFontSize: 12,
        showUnselectedLabels: true,
        elevation: 0,
      ),
    );
  }
}
