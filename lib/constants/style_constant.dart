//style for title
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:google_fonts/google_fonts.dart';

import 'color_constant.dart';

var mTitleHeader = GoogleFonts.inter(
    fontWeight: FontWeight.w600, color: Colors.black26, fontSize: 30);

var mTitleHeaderSm = GoogleFonts.inter(
    fontWeight: FontWeight.w400, color: Colors.black26, fontSize: 18);

var mTitleHeaderScaffold = GoogleFonts.inter(
    fontWeight: FontWeight.w400, color: Colors.white, fontSize: 18);

var mTitleHeaderSub = GoogleFonts.inter(
    fontWeight: FontWeight.w600, color: Colors.blueAccent, fontSize: 30);

var mTitleStyle = GoogleFonts.inter(
    fontWeight: FontWeight.w600, color: Colors.black26, fontSize: 14);

var mTitleStyleSm = GoogleFonts.inter(
    fontWeight: FontWeight.w400, color: Colors.black26, fontSize: 12);

var mServiceTitleStyle = GoogleFonts.inter(
    fontWeight: FontWeight.w600, color: mSubtitleColor, fontSize: 12);

var mServiceSubtitleStyle = GoogleFonts.inter(
    fontWeight: FontWeight.w300, color: mSubtitleColor, fontSize: 11);

var mDesTitleStyle = GoogleFonts.inter(
    fontWeight: FontWeight.w600, color: mTitleColor, fontSize: 14);

var mDesSubtitleStyle = GoogleFonts.inter(
    fontWeight: FontWeight.w200, color: mSubtitleColor, fontSize: 11);

var mTravTitleStyle = GoogleFonts.inter(
    fontWeight: FontWeight.w600, color: mFillColor, fontSize: 10);

var mTravSubtitleStyle = GoogleFonts.inter(
    fontWeight: FontWeight.w200, color: mSubtitleColor, fontSize: 11);

var mTravPlaceStyle = GoogleFonts.inter(
    fontWeight: FontWeight.w600, color: mCardTitleColor, fontSize: 12);
