import 'package:flutter/material.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

// ignore: must_be_immutable
class DetailMateri extends StatefulWidget {
  String title;
  String path;
  bool video;

  DetailMateri(
      {Key? key, required this.title, required this.path, required this.video})
      : super(key: key);

  @override
  _DetailMateriState createState() => _DetailMateriState();
}

class _DetailMateriState extends State<DetailMateri> {
  final List<YoutubePlayerController> _controllers = [
    'WzvsPzIxt1g',
    '14CPu0ikn4w',
  ]
      .map<YoutubePlayerController>(
        (videoId) => YoutubePlayerController(
          initialVideoId: videoId,
          flags: const YoutubePlayerFlags(
            autoPlay: false,
          ),
        ),
      )
      .toList();

  late PdfViewerController _pdfViewerController;
  final GlobalKey<SfPdfViewerState> _pdfViewerStateKey = GlobalKey();
  @override
  void initState() {
    _pdfViewerController = PdfViewerController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var params = (widget.title == 'SEJARAH MOTIF KEMBANG DANGAR' ? 0 : 1);
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: ListView(
          padding: const EdgeInsets.all(8),
          children: <Widget>[
            Container(
              height: (widget.video ? 345 : 550),
              padding: const EdgeInsets.only(bottom: 5.0),
              child: SfPdfViewer.asset(widget.path,
                  controller: _pdfViewerController, key: _pdfViewerStateKey),
            ),
            (widget.video
                ? Container(
                    height: 200,
                    color: Colors.amber[500],
                    child: YoutubePlayer(
                      key: ObjectKey(_controllers[params]),
                      controller: _controllers[params],
                      actionsPadding: const EdgeInsets.only(left: 16.0),
                      bottomActions: [
                        CurrentPosition(),
                        const SizedBox(width: 10.0),
                        ProgressBar(isExpanded: true),
                        const SizedBox(width: 10.0),
                        RemainingDuration(),
                        FullScreenButton(),
                      ],
                    ),
                  )
                : Container())
          ],
        ));
  }
}
