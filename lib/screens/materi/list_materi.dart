import 'package:dangar/constants/style_constant.dart';
import 'package:dangar/screens/materi/materi_detail.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class ListMateriScreen extends StatefulWidget {
  const ListMateriScreen({Key? key}) : super(key: key);

  @override
  State<ListMateriScreen> createState() => _ListMateriScreenState();
}

class _ListMateriScreenState extends State<ListMateriScreen> {
  final List<Map<String, dynamic>> materis = [
    {
      "title": "PETA KONSEP DAN PETUNJUK BELAJAR",
      "path": "assets/files/PETA KONSEP DAN PETUNJUK BELAJAR.pdf",
      "video": false,
    },
    {
      "title": "SENI RUPA DUA DIMENSI",
      "path": "assets/files/SENI RUPA DUA DIMENSI.pdf",
      "video": false,
    },
    {
      "title": "SEJARAH MOTIF KEMBANG DANGAR",
      "path": "assets/files/SEJARAH MOTIF KEMBANG DANGAR.pdf",
      "video": true,
    },
    {
      "title": "BENTUK DAN MAKNA",
      "path": "assets/files/BENTUK DAN MAKNA.pdf",
      "video": false,
    },
    {
      "title": "PENGAPLIKASIAN",
      "path": "assets/files/PENGAPLIKASIAN.pdf",
      "video": false,
    },
    {
      "title": "ALAT, BAHAN DAN TEKNIK PEMBUATAN",
      "path": "assets/files/ALAT, BAHAN DAN TEKNIK PEMBUATAN.pdf",
      "video": true,
    },
    {
      "title": "KAJIAN MOTIF KEMBANG DANGAR",
      "path": "assets/files/KAJIAN MOTIF KEMBANG DANGAR.pdf",
      "video": false,
    },
    {
      "title": "TUGAS KELOMPOK",
      "path": "assets/files/TUGAS KELOMPOK.pdf",
      "video": false,
    },
    {
      "title": "TUGAS MANDIRI",
      "path": "assets/files/TUGAS MANDIRI.pdf",
      "video": false,
    },
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("DAFTAR MATERI", style: mTitleHeaderScaffold),
      ),
      body: ListView.builder(
        itemCount: materis.length,
        itemBuilder: (context, i) {
          return GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => DetailMateri(
                            title: materis[i]['title'].toString(),
                            path: materis[i]['path'].toString(),
                            video: materis[i]['video'])));
              },
              child: Container(
                  padding: const EdgeInsets.only(bottom: 3.0),
                  child: Card(
                    color: Colors.white,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 18.0, horizontal: 16.0),
                      child: Text(materis[i]['title'].toString(),
                          style: mTitleStyle),
                    ),
                  )));
        },
      ),
    );
  }
}
