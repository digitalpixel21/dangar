import 'dart:convert';

import 'package:dangar/network_utils/api.dart';
import 'package:dangar/widgets/bottom_tab_initial.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:dangar/constants/colors.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:shared_preferences/shared_preferences.dart';

// ignore: must_be_immutable
class ResultScreen extends StatefulWidget {
  int score;
  ResultScreen(this.score, {Key? key}) : super(key: key);

  @override
  _ResultScreenState createState() => _ResultScreenState();
}

class _ResultScreenState extends State<ResultScreen> {
  // ignore: prefer_typing_uninitialized_variables
  var userData;
  @override
  void initState() {
    _getUserInfo();
    super.initState();
  }

  void _getUserInfo() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var userJson = localStorage.getString('user');
    var user = json.decode(userJson);
    setState(() {
      userData = user;
    });
  }

  _showMsg(msg) {
    final snackBar = SnackBar(
      content: Text(msg),
      action: SnackBarAction(
        label: 'Close',
        onPressed: () {
          // Some code to undo the change!
        },
      ),
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.pripmaryColor,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const SizedBox(
            width: double.infinity,
            child: Text(
              "Selamat",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.white,
                fontSize: 40.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          const SizedBox(
            height: 45.0,
          ),
          const Text(
            "Skormu adalah",
            style: TextStyle(color: Colors.white, fontSize: 34.0),
          ),
          const SizedBox(
            height: 20.0,
          ),
          Text(
            "${widget.score * 10}",
            style: const TextStyle(
              color: Colors.orange,
              fontSize: 85.0,
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(
            height: 100.0,
          ),
          TextButton(
            onPressed: () {
              _sendScores();
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const BottomTabInitial(),
                  ));
            },
            style: ButtonStyle(
              padding: MaterialStateProperty.all(const EdgeInsets.all(5.0)),
            ),
            child: Ink(
              decoration: BoxDecoration(
                  gradient: const LinearGradient(
                    colors: [Color(0xff117eeb), Color(0xffdb7285)],
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                  ),
                  borderRadius: BorderRadius.circular(30.0)),
              child: Container(
                constraints:
                    const BoxConstraints(maxWidth: 300.0, minHeight: 50.0),
                alignment: Alignment.center,
                child: const Text(
                  "Selesai",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _sendScores() async {
    var data = {'id': userData['user_id'], 'skor': widget.score * 10};

    var res = await CallApi().postData(data, 'skor');
    var body = json.decode(res.body);
    if (body['success']) {
      // ignore: avoid_print
      print('berhasil simpan skor');
    } else {
      _showMsg(body['message']);
    }
    _updatePretest();
  }

  void _updatePretest() async {
    var data = {'id': userData['user_id']};

    var res = await CallApi().postData(data, 'cekPretest');
    var body = json.decode(res.body);
    if (body['success']) {
      // ignore: avoid_print
      print('update pretest berhasil');
    } else {
      _showMsg('terjadi kesalahan saat logout');
    }
  }
}
