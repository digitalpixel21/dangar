// ignore_for_file: prefer_const_constructors
import 'dart:convert';
import 'package:dangar/screens/materi/list_materi.dart';
import 'package:dangar/screens/menu/pendahuluan.dart';
import 'package:dangar/screens/menu/petunjuk.dart';
import 'package:dangar/screens/menu/profil.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:dangar/constants/color_constant.dart';
import 'package:dangar/constants/style_constant.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:shared_preferences/shared_preferences.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:url_launcher/url_launcher.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  // ignore: prefer_typing_uninitialized_variables
  var userData;
  @override
  void initState() {
    _getUserInfo();
    super.initState();
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  void _getUserInfo() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var userJson = localStorage.getString('user');
    var user = json.decode(userJson);
    setState(() {
      userData = user;
    });
  }

  int _current = 0;

  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }
    return result;
  }

  final List<Map<String, String>> carousels = [
    {"image": "assets/images/BANNER01.jpg"},
    {"image": "assets/images/BANNER02.jpg"},
  ];

  final List<Map<String, String>> travelogs = [
    {
      "name":
          "Motif Batik Kembang Dangar Khas Lombok Utara Ternyata Punya Nilai Sejarah yang Tinggi",
      "image": "assets/images/berita1.jpg",
      "url":
          "https://bukadikit.co/motif-batik-kembang-dangar-khas-lombok-utara-ternyata-punya-nilai-sejarah-yang-tinggi",
      "content":
          "Lombok Utara, bukadikit.co– Baru-baru ini Pemerintah Daerah Lombok Utara melaunching motif batik Kembang Dangar khas Lombok Utara saat peringatan HUT RI ke-75 pada 17 Agustus lalu. Motif kembang dangar ini memang merupakan motif batik khas pertama yang dimiliki oleh Kabupaten Lombok Utara."
    },
    {
      "name":
          "HUT RI ke-75, Pemda Launching “Sapuk Kembang Dangar” Khas Lombok Utara",
      "image": "assets/images/berita2.jpg",
      "url":
          "https://bukadikit.co/hut-ri-ke-75-pemda-launching-sapuk-kembang-dangar-khas-lombok-utara",
      "content":
          "Lombok Utara, bukadikit.co– Di hari peringatan kemerdekaan Republik Indonesia (RI) yang ke-75, pemerintah daerah Kabupaten Lombok Utara me-launching sapuk dengan motif batik khas Lombok Utara yang dinamakan Kembang Dangar."
    },
    {
      "name":
          "500 Pohon di Pusuk Bakal Ditebang Salah Satunya Pohon Dangar, Aktivis Lingkungan dan Budayawan Adakan Aksi dan Ritual",
      "image": "assets/images/berita3.jpg",
      "url":
          "https://www.suarabumigora.com/2021/07/500-pohon-di-pusuk-bakal-ditebang.html",
      "content":
          "Lombok Utara, suarabumigora.com - Rencana pelebaran ruas jalan Rembiga-Pemenang menuai pro dan kontra di kalangan masyarakat. Beberapa hari terakhir, masyarakat dikejutkan dengan beredarnya surat dari PT Seruyan Sampurna dengan tujuan meminta bantuan personel untuk pengamanan dan sosialisasi penebangan pohon di hutan lindung Pusuk. Surat tersebut dengan cepat diresposn oleh berbagai pengguna media sosial dan mendapat berbagai tanggapan, beberapa di antaranya dari aktivis lingkungan dan budayawan."
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //bg Color
      backgroundColor: mBackgroundColor,
      // Setting up appbar
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Image.asset(
          'assets/svg/akar.png',
          width: 80,
        ),
        automaticallyImplyLeading: false,
        elevation: 0,
      ),

      //BottomNav
      // bottomNavigationBar: BottomTabInitial(),

      //Body
      body: ListView(
        physics: ClampingScrollPhysics(),
        children: <Widget>[
          //Slider Section
          Padding(
            padding: const EdgeInsets.only(left: 16, right: 16, bottom: 16),
            child: Text(
              // 'Hi, Selamat Datang Mujur!!',
              userData != null ? 'Hi, Selamat Datang ${userData['name']}' : '',
              style: mTitleStyle,
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            margin: EdgeInsets.only(left: 16, right: 16),
            width: MediaQuery.of(context).size.width,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(
                  width: MediaQuery.of(context).size.width,
                  height: 200,
                  child: Swiper(
                    onIndexChanged: (index) {
                      setState(() {
                        _current = index;
                      });
                    },
                    autoplay: true,
                    layout: SwiperLayout.DEFAULT,
                    itemCount: carousels.length,
                    itemBuilder: (BuildContext context, index) {
                      return Container(
                          decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        image: DecorationImage(
                            image: AssetImage(
                                carousels[index]['image'].toString()),
                            fit: BoxFit.fill),
                      ));
                    },
                  ),
                ),
                SizedBox(height: 12),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: map(carousels, (index, image) {
                        return Container(
                          alignment: Alignment.centerLeft,
                          height: 6,
                          width: 6,
                          margin: EdgeInsets.only(right: 8),
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color:
                                  _current == index ? mBlueColor : mGreyColor),
                        );
                      }),
                    ),
                    //More
                    Text(
                      'Lainnya',
                      style: mTitleStyle,
                    )
                  ],
                )
              ],
            ),
          ),

          //Menu Section
          Padding(
            padding: EdgeInsets.only(left: 16, top: 16, bottom: 10),
            child: Text(
              "Menu",
              style: mTitleStyle,
            ),
          ),
          Container(
            height: 144,
            margin: EdgeInsets.only(left: 16, right: 16),
            child: Column(
              children: [
                Row(
                  children: [
                    Expanded(
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => Petunjuk()),
                          );
                        },
                        child: Container(
                          margin: EdgeInsets.only(right: 8),
                          padding: EdgeInsets.only(left: 16),
                          height: 64,
                          decoration: BoxDecoration(
                              color: mFillColor,
                              borderRadius: BorderRadius.circular(12),
                              border:
                                  Border.all(color: mBorderColor, width: 1)),
                          child: Row(
                            children: [
                              SvgPicture.asset(
                                'assets/svg/petunjuk.svg',
                                fit: BoxFit.contain,
                              ),
                              Padding(
                                padding: EdgeInsets.only(left: 14, right: 14),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text('Petunjuk', style: mServiceTitleStyle),
                                    Text('Aplikasi',
                                        style: mServiceSubtitleStyle)
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => Pendahuluan()),
                          );
                        },
                        child: Container(
                          margin: EdgeInsets.only(right: 8),
                          padding: EdgeInsets.only(left: 16),
                          height: 64,
                          decoration: BoxDecoration(
                              color: mFillColor,
                              borderRadius: BorderRadius.circular(12),
                              border:
                                  Border.all(color: mBorderColor, width: 1)),
                          child: Row(
                            children: [
                              SvgPicture.asset(
                                'assets/svg/pendahuluan.svg',
                                fit: BoxFit.contain,
                              ),
                              Padding(
                                padding: EdgeInsets.only(left: 14, right: 14),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text('Pendahuluan',
                                        style: mServiceTitleStyle),
                                    Text('Esai', style: mServiceSubtitleStyle)
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 16),
                Row(
                  children: [
                    Expanded(
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ListMateriScreen()),
                          );
                        },
                        child: Container(
                          margin: EdgeInsets.only(right: 8),
                          padding: EdgeInsets.only(left: 16),
                          height: 64,
                          decoration: BoxDecoration(
                              color: mFillColor,
                              borderRadius: BorderRadius.circular(12),
                              border:
                                  Border.all(color: mBorderColor, width: 1)),
                          child: Row(
                            children: [
                              SvgPicture.asset(
                                'assets/svg/materi.svg',
                                fit: BoxFit.contain,
                              ),
                              Padding(
                                padding: EdgeInsets.only(left: 14, right: 14),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text('Materi', style: mServiceTitleStyle),
                                    Text('Ayo Belajar',
                                        style: mServiceSubtitleStyle)
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => Profil()),
                          );
                        },
                        child: Container(
                          margin: EdgeInsets.only(right: 8),
                          padding: EdgeInsets.only(left: 16),
                          height: 64,
                          decoration: BoxDecoration(
                              color: mFillColor,
                              borderRadius: BorderRadius.circular(12),
                              border:
                                  Border.all(color: mBorderColor, width: 1)),
                          child: Row(
                            children: [
                              SvgPicture.asset(
                                'assets/svg/profil.svg',
                                fit: BoxFit.contain,
                              ),
                              Padding(
                                padding: EdgeInsets.only(left: 14, right: 14),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text('Profil', style: mServiceTitleStyle),
                                    Text('Pengembang',
                                        style: mServiceSubtitleStyle)
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),

          //Artikel Section
          Padding(
            padding: EdgeInsets.only(left: 16, top: 16, bottom: 10),
            child: Text(
              "Artikel",
              style: mTitleStyle,
            ),
          ),
          SizedBox(
            height: 200,
            child: ListView.builder(
              itemCount: travelogs.length,
              scrollDirection: Axis.horizontal,
              padding: EdgeInsets.only(left: 16),
              itemBuilder: (context, index) {
                return Container(
                  margin: EdgeInsets.only(right: 16),
                  width: 220,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Stack(
                        children: [
                          GestureDetector(
                            onTap: () {
                              _launchURL(travelogs[index]['url'].toString());
                            },
                            child: Container(
                              height: 120,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(8),
                                  image: DecorationImage(
                                      image: AssetImage(
                                          travelogs[index]['image'].toString()),
                                      fit: BoxFit.cover)),
                            ),
                          ),
                          Positioned(
                            child: SvgPicture.asset(
                                'assets/svg/travlog_top_corner.svg'),
                            right: 0,
                          ),
                          Positioned(
                            child: Image.asset(
                              'assets/svg/AKAR-PUTIH.png',
                              height: 15,
                            ),
                            right: 8,
                            top: 8,
                          ),
                          Positioned(
                              child: SvgPicture.asset(
                                  'assets/svg/travlog_bottom_gradient.svg'),
                              bottom: 0),
                          Positioned(
                            child: Text(travelogs[index]['name'].toString(),
                                style: mTravTitleStyle,
                                overflow: TextOverflow.clip),
                            bottom: 8,
                            left: 8,
                          )
                        ],
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Text(travelogs[index]['content'].toString(),
                          maxLines: 3,
                          textAlign: TextAlign.justify,
                          style: mTravSubtitleStyle),
                      SizedBox(
                        height: 8,
                      ),
                    ],
                  ),
                );
              },
            ),
          )
        ],
      ),
    );
  }
}
