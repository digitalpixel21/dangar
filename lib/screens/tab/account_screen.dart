import 'dart:convert';

import 'package:dangar/constants/color_constant.dart';
import 'package:dangar/constants/style_constant.dart';
import 'package:dangar/models/nilai_model.dart';
import 'package:dangar/network_utils/api.dart';
import 'package:dangar/screens/login_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:shared_preferences/shared_preferences.dart';

class AccountScreen extends StatefulWidget {
  const AccountScreen({Key? key}) : super(key: key);

  @override
  MapScreenState createState() => MapScreenState();
}

class MapScreenState extends State<AccountScreen>
    with SingleTickerProviderStateMixin {
  // ignore: prefer_typing_uninitialized_variables
  var userData;
  bool _status = true;
  final FocusNode myFocusNode = FocusNode();
  // ignore: prefer_typing_uninitialized_variables
  var kuis;
  bool _isLoading = false;
  int nomor = 0;

  TextEditingController namaController = TextEditingController();
  TextEditingController usernameController = TextEditingController();
  TextEditingController sekolahController = TextEditingController();
  TextEditingController kelasController = TextEditingController();
  TextEditingController alamatController = TextEditingController();
  TextEditingController sexController = TextEditingController();
  TextEditingController tglController = TextEditingController();

  late List<Nilai> list = [];

  @override
  void initState() {
    super.initState();
    _loadData();
  }

  _showMsg(msg) {
    //
    final snackBar = SnackBar(
      content: Text(msg),
      action: SnackBarAction(
        label: 'Close',
        onPressed: () {
          // Some code to undo the change!
        },
      ),
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  _loadData() async {
    setState(() {
      _isLoading = true;
    });
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var userJson = prefs.getString('user');
    var user = json.decode(userJson);
    setState(() {
      userData = user;
    });
    namaController = TextEditingController(text: userData['name']);
    usernameController = TextEditingController(text: userData['username']);
    sekolahController = TextEditingController(text: userData['school']);
    kelasController = TextEditingController(text: userData['class']);
    alamatController = TextEditingController(text: userData['address']);
    sexController = TextEditingController(text: userData['sex']);
    tglController = TextEditingController(text: userData['date']);

    _getTable();

    setState(() {
      _isLoading = false;
    });
  }

  _getTable() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString("pretest", '1');
    var userJson = prefs.getString('user');
    var user = json.decode(userJson);
    setState(() {
      userData = user;
    });
    var data = {
      'user_id': userData['user_id'],
    };

    var res = await CallApi().postData(data, 'histori_kuis');
    var body = json.decode(res.body)['data'] as List;
    List<Nilai> varList =
        body.map((userJson) => Nilai.fromJson(userJson)).toList();

    setState(() {
      list = varList;
    });
  }

  void _updateProfil() async {
    setState(() {
      _isLoading = true;
    });

    var data = {
      'id': userData['user_id'],
      'nama': namaController.text,
      'username': usernameController.text,
      'sekolah': sekolahController.text,
      'kelas': kelasController.text,
      'alamat': alamatController.text,
    };

    var res = await CallApi().postData(data, 'profil');
    var body = json.decode(res.body);
    if (body['success']) {
      SharedPreferences localStorage = await SharedPreferences.getInstance();
      localStorage.setString('user', json.encode(body['profil']));
    }

    setState(() {
      _isLoading = false;
    });
  }

  void _logout() async {
    setState(() {
      _isLoading = true;
    });

    var data = {'id': userData['user_id']};

    var res = await CallApi().postData(data, 'logout');
    var body = json.decode(res.body);
    if (body['success']) {
      SharedPreferences localStorage = await SharedPreferences.getInstance();
      localStorage.remove('user');
      localStorage.remove('token');
      localStorage.remove('pretest');
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => const Login()));
    } else {
      _showMsg('Terjadi kesalahan saat logout');
    }

    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    // ignore: unnecessary_new
    return new Scaffold(
        body: Container(
      color: Colors.white,
      child: _isLoading
          ? const Center(
              child: CircularProgressIndicator(),
            )
          : ListView(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Container(
                      color: const Color(0xffFFFFFF),
                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 25.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 25.0, right: 25.0, top: 25.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      Text(
                                        'Informasi Pribadi',
                                        style: mTitleHeaderSm,
                                      ),
                                    ],
                                  ),
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      _status ? _getEditIcon() : Container(),
                                    ],
                                  )
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 25.0, right: 25.0, top: 25.0),
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Expanded(
                                    child: Text(
                                      'Nama',
                                      style: mTitleStyle,
                                    ),
                                    flex: 2,
                                  ),
                                  Expanded(
                                    child: Text(
                                      'Username',
                                      style: mTitleStyle,
                                    ),
                                    flex: 2,
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 25.0, right: 25.0, top: 2.0),
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Flexible(
                                    child: Padding(
                                      padding:
                                          const EdgeInsets.only(right: 10.0),
                                      child: TextField(
                                        controller: namaController,
                                        style: mTitleStyle,
                                        decoration: const InputDecoration(
                                          hintText: "Masukan nama anda",
                                        ),
                                        enabled: !_status,
                                      ),
                                    ),
                                    flex: 2,
                                  ),
                                  Flexible(
                                    child: TextField(
                                      controller: usernameController,
                                      style: mTitleStyle,
                                      decoration: const InputDecoration(
                                          hintText: "Masukan username"),
                                      enabled: !_status,
                                    ),
                                    flex: 2,
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 25.0, right: 25.0, top: 25.0),
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Expanded(
                                    child: Text(
                                      'Sekolah',
                                      style: mTitleStyle,
                                    ),
                                    flex: 2,
                                  ),
                                  Expanded(
                                    child: Text(
                                      'Kelas',
                                      style: mTitleStyle,
                                    ),
                                    flex: 2,
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 25.0, right: 25.0, top: 2.0),
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Flexible(
                                    child: Padding(
                                      padding:
                                          const EdgeInsets.only(right: 10.0),
                                      child: TextField(
                                        controller: sekolahController,
                                        style: mTitleStyle,
                                        decoration: const InputDecoration(
                                            hintText: "Masukan sekolah"),
                                        enabled: !_status,
                                      ),
                                    ),
                                    flex: 2,
                                  ),
                                  Flexible(
                                    child: TextField(
                                      controller: kelasController,
                                      style: mTitleStyle,
                                      decoration: const InputDecoration(
                                          hintText: "Masukan kelas"),
                                      enabled: !_status,
                                    ),
                                    flex: 2,
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                                padding: const EdgeInsets.only(
                                    left: 25.0, right: 25.0, top: 25.0),
                                child: Row(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Expanded(
                                      child: Text(
                                        'Jenis Kelamin',
                                        style: mTitleStyle,
                                      ),
                                      flex: 2,
                                    ),
                                    Expanded(
                                      child: Text(
                                        'Tanggal Daftar',
                                        style: mTitleStyle,
                                      ),
                                      flex: 2,
                                    ),
                                  ],
                                )),
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 25.0, right: 25.0, top: 2.0),
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Flexible(
                                    child: Padding(
                                      padding:
                                          const EdgeInsets.only(right: 10.0),
                                      child: TextField(
                                        controller: sexController,
                                        style: mTitleStyle,
                                        decoration: const InputDecoration(
                                            hintText: "Pilih jenis kelamin"),
                                        enabled: false,
                                      ),
                                    ),
                                    flex: 2,
                                  ),
                                  Flexible(
                                    child: TextField(
                                      controller: tglController,
                                      style: mTitleStyle,
                                      decoration: const InputDecoration(
                                          hintText: "Masukan tanggal"),
                                      enabled: false,
                                    ),
                                    flex: 2,
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                                padding: const EdgeInsets.only(
                                    left: 25.0, right: 25.0, top: 25.0),
                                child: Row(
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        Text('Alamat', style: mTitleStyle),
                                      ],
                                    ),
                                  ],
                                )),
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 25.0, right: 25.0, top: 2.0),
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  Flexible(
                                    child: TextField(
                                      controller: alamatController,
                                      style: mTitleStyle,
                                      decoration: const InputDecoration(
                                          hintText: "Masukan alamat"),
                                      enabled: !_status,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 25.0, right: 25.0, top: 25.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      Text(
                                        'Riwayat Hasil Kuis',
                                        style: mTitleHeaderSm,
                                      ),
                                    ],
                                  ),
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    mainAxisSize: MainAxisSize.min,
                                    children: const <Widget>[
                                      CircleAvatar(
                                        backgroundColor: mBlueColor,
                                        radius: 14.0,
                                        child: Icon(
                                          Icons.calendar_today,
                                          color: Colors.white,
                                          size: 16.0,
                                        ),
                                      )
                                    ],
                                  )
                                ],
                              ),
                            ),
                            (list.isNotEmpty
                                ? Padding(
                                    padding: const EdgeInsets.only(
                                        left: 5.0, right: 5.0, top: 10.0),
                                    child: SingleChildScrollView(
                                      scrollDirection: Axis.horizontal,
                                      child: SingleChildScrollView(
                                        child: DataTable(
                                          columns: [
                                            DataColumn(
                                                label: Text('Tanggal',
                                                    style: mTitleStyle)),
                                            DataColumn(
                                                label: Text('Nilai',
                                                    style: mTitleStyle)),
                                          ],
                                          rows: list
                                              .map((data) => DataRow(cells: [
                                                    DataCell(SizedBox(
                                                      width: 200,
                                                      child: Text(
                                                          data.tanggal
                                                              .toString(),
                                                          style: mTitleStyle),
                                                    )),
                                                    DataCell(Text(
                                                        data.nilai.toString(),
                                                        style: mTitleStyle)),
                                                  ]))
                                              .toList(),
                                        ),
                                      ),
                                    ),
                                  )
                                : Container()),
                            !_status ? _getActionButtons() : _getLogout(),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ),
    ));
  }

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    myFocusNode.dispose();
    super.dispose();
  }

  Widget _getLogout() {
    return Padding(
      padding: const EdgeInsets.only(left: 25.0, right: 25.0, top: 45.0),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 10.0),
              child: ElevatedButton(
                child: const Text("Keluar"),
                style: ElevatedButton.styleFrom(
                  primary: Colors.red,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20.0)),
                ),
                onPressed: () {
                  _logout();
                  setState(() {
                    _status = true;
                    FocusScope.of(context).requestFocus(FocusNode());
                  });
                },
              ),
            ),
            flex: 2,
          ),
        ],
      ),
    );
  }

  Widget _getActionButtons() {
    return Padding(
      padding: const EdgeInsets.only(left: 25.0, right: 25.0, top: 45.0),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(right: 10.0),
              child: ElevatedButton(
                child: const Text("Simpan"),
                style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20.0)),
                ),
                onPressed: () {
                  _updateProfil();
                  setState(() {
                    _status = true;
                    FocusScope.of(context).requestFocus(FocusNode());
                  });
                },
              ),
            ),
            flex: 2,
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 10.0),
              child: ElevatedButton(
                child: const Text("Batal"),
                style: ElevatedButton.styleFrom(
                  primary: Colors.red,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20.0)),
                ),
                onPressed: () {
                  _loadData();
                  setState(() {
                    _status = true;
                    FocusScope.of(context).requestFocus(FocusNode());
                  });
                },
              ),
            ),
            flex: 2,
          ),
        ],
      ),
    );
  }

  Widget _getEditIcon() {
    return GestureDetector(
      child: const CircleAvatar(
        backgroundColor: mBlueColor,
        radius: 14.0,
        child: Icon(
          Icons.edit,
          color: Colors.white,
          size: 16.0,
        ),
      ),
      onTap: () {
        setState(() {
          _status = false;
        });
      },
    );
  }

  countIndex() {
    setState(() {
      nomor++;
    });
  }
}
