// ignore_for_file: unnecessary_brace_in_string_interps

import 'dart:math';

import 'package:flutter/material.dart';
import 'package:dangar/data/questions.dart';
import 'package:dangar/screens/result/result_screen.dart';
import 'package:dangar/constants/colors.dart';

class PretestScreen extends StatefulWidget {
  const PretestScreen({Key? key}) : super(key: key);

  @override
  _PretestScreenState createState() => _PretestScreenState();
}

class _PretestScreenState extends State<PretestScreen> {
  int score = 0;
  bool cek = false;
  PageController? _controller;
  String btnText = "Selanjutnya";
  bool answered = false;

  Map<String, Color> btncolor = {
    "0": AppColor.secondaryColor,
    "1": AppColor.secondaryColor,
    "2": AppColor.secondaryColor,
    "3": AppColor.secondaryColor,
    "4": AppColor.secondaryColor,
    "5": AppColor.secondaryColor,
  };

  Map<String, bool> btnSelected = {
    "0": false,
    "1": false,
    "2": false,
    "3": false,
    "4": false,
  };

  @override
  void initState() {
    super.initState();
    _controller = PageController(initialPage: 0);
    shuffle(questions);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.pripmaryColor,
      body: Padding(
          padding: const EdgeInsets.all(18.0),
          child: PageView.builder(
            controller: _controller!,
            onPageChanged: (page) {
              if (page == questions.length - 1) {
                setState(() {
                  btnText = "Lihat Hasil";
                });
              }
              setState(() {
                answered = false;
              });
            },
            physics: const NeverScrollableScrollPhysics(),
            itemBuilder: (context, index) {
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    width: double.infinity,
                    child: Text(
                      "Pertanyaan ${index + 1}/10",
                      textAlign: TextAlign.start,
                      style: const TextStyle(
                        color: Colors.white,
                        fontSize: 20.0,
                      ),
                    ),
                  ),
                  const Divider(
                    color: Colors.white,
                  ),
                  const SizedBox(
                    height: 10.0,
                  ),
                  SizedBox(
                    width: double.infinity,
                    height: 170.0,
                    child: Text(
                      "${questions[index].question}",
                      style: const TextStyle(
                        color: Colors.white,
                        fontSize: 18.0,
                      ),
                    ),
                  ),
                  for (int i = 0; i < questions[index].answers!.length; i++)
                    Container(
                      width: double.infinity,
                      height: 35.0,
                      margin: const EdgeInsets.only(
                          bottom: 15.0, left: 10.0, right: 10.0),
                      child: RawMaterialButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8.0),
                        ),
                        fillColor: btnSelected['${i}']!
                            ? Colors.green
                            : btncolor['${i}'],
                        onPressed: !answered
                            ? () {
                                if (questions[index]
                                    .answers!
                                    .values
                                    .toList()[i]) {
                                  score++;
                                  // ignore: avoid_print
                                  print("yes");
                                } else {
                                  // ignore: avoid_print
                                  print("no");
                                }
                                setState(() {
                                  btnSelected['${i}'] = true;
                                  answered = true;
                                });
                              }
                            : null,
                        child: Text(questions[index].answers!.keys.toList()[i],
                            style: const TextStyle(
                              color: Colors.white,
                              fontSize: 18.0,
                            )),
                      ),
                    ),
                  const SizedBox(
                    height: 10.0,
                  ),
                  answered
                      ? RawMaterialButton(
                          onPressed: () {
                            if (_controller!.page?.toInt() ==
                                questions.length - 1) {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          ResultScreen(score)));
                            } else {
                              _controller!.nextPage(
                                  duration: const Duration(milliseconds: 250),
                                  curve: Curves.easeInExpo);

                              btnSelected.updateAll((key, value) => false);

                              setState(() {
                                answered = false;
                              });
                            }
                          },
                          shape: const StadiumBorder(),
                          fillColor: Colors.blue,
                          padding: const EdgeInsets.all(13.0),
                          elevation: 0.0,
                          child: Text(
                            btnText,
                            style: const TextStyle(color: Colors.white),
                          ),
                        )
                      : Container(),
                ],
              );
            },
            itemCount: questions.length,
          )),
    );
  }
}

List shuffle(List items) {
  var random = Random();

  // Go through all elements.
  for (var i = items.length - 1; i > 0; i--) {
    // Pick a pseudorandom number according to the list length
    var n = random.nextInt(i + 1);

    var temp = items[i];
    items[i] = items[n];
    items[n] = temp;
  }

  return items;
}
