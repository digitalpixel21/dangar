import 'package:flutter/material.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

class Petunjuk extends StatefulWidget {
  const Petunjuk({Key? key}) : super(key: key);

  @override
  _PetunjukState createState() => _PetunjukState();
}

class _PetunjukState extends State<Petunjuk> {
  late PdfViewerController _pdfViewerController;
  final GlobalKey<SfPdfViewerState> _pdfViewerStateKey = GlobalKey();
  @override
  void initState() {
    _pdfViewerController = PdfViewerController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: SfPdfViewer.asset('assets/files/PETUNJUK PENGGUNAAN.pdf',
          controller: _pdfViewerController, key: _pdfViewerStateKey),
      appBar: AppBar(
        title: const Text(
          'Petunjuk',
        ),
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: const Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
        ),
      ),
    ));
  }
}
