import 'package:flutter/material.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

class Pendahuluan extends StatefulWidget {
  const Pendahuluan({Key? key}) : super(key: key);

  @override
  _PendahuluanState createState() => _PendahuluanState();
}

class _PendahuluanState extends State<Pendahuluan> {
  late PdfViewerController _pdfViewerController;
  final GlobalKey<SfPdfViewerState> _pdfViewerStateKey = GlobalKey();
  @override
  void initState() {
    _pdfViewerController = PdfViewerController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: SfPdfViewer.asset('assets/files/PENDAHULUAN.pdf',
          controller: _pdfViewerController, key: _pdfViewerStateKey),
      appBar: AppBar(
        title: const Text(
          'Pendahuluan',
        ),
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: const Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
        ),
      ),
    ));
  }
}
