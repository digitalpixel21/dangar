import 'dart:convert';

import 'package:dangar/network_utils/api.dart';
import 'package:dangar/screens/login_screen.dart';
import 'package:flutter/material.dart';
import 'package:dangar/constants/style_constant.dart';
import 'package:dangar/widgets/bezierContainer.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:shared_preferences/shared_preferences.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:dangar/main.dart';

class SignUp extends StatefulWidget {
  final String title;
  // ignore: use_key_in_widget_constructors
  const SignUp({this.title = ""});

  @override
  _SignUpState createState() => _SignUpState();
}

class JKList {
  String title;
  String jk;
  int index;
  JKList({required this.title, required this.jk, required this.index});
}

// ignore: constant_identifier_names
enum SingingCharacter { laki_laki, perempuan }

class _SignUpState extends State<SignUp> {
  bool _isLoading = false;

  TextEditingController namaController = TextEditingController();
  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController sekolahController = TextEditingController();
  TextEditingController kelasController = TextEditingController();
  TextEditingController alamatController = TextEditingController();

  // Default Radio Button Selected Item.
  String jenisKelamin = 'laki_laki';

  // Group Value for Radio Button.
  int id = 1;

  List<JKList> nList = [
    JKList(
      title: 'Laki-laki',
      index: 1,
      jk: "laki_laki",
    ),
    JKList(
      title: 'Perempuan',
      index: 2,
      jk: "perempuan",
    ),
  ];

  Widget _submitButton() {
    return SizedBox(
      height: 50.0,
      child: TextButton(
        onPressed: _isLoading ? null : _signup,
        style: ButtonStyle(
          padding: MaterialStateProperty.all(const EdgeInsets.all(0.0)),
        ),
        child: Ink(
          decoration: BoxDecoration(
              gradient: const LinearGradient(
                colors: [Color(0xff117eeb), Color(0xffdb7285)],
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
              ),
              borderRadius: BorderRadius.circular(30.0)),
          child: Container(
            constraints: const BoxConstraints(maxWidth: 300.0, minHeight: 50.0),
            alignment: Alignment.center,
            child: Text(
              _isLoading ? 'Menunggu...' : 'Daftar',
              textAlign: TextAlign.center,
              style: const TextStyle(color: Colors.white),
            ),
          ),
        ),
      ),
    );
  }

  Widget _divider() {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 3),
      child: Row(
        children: <Widget>[
          const SizedBox(
            width: 20,
          ),
          const Expanded(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Divider(
                thickness: 1,
              ),
            ),
          ),
          Text(
            'atau',
            style: mServiceTitleStyle,
          ),
          const Expanded(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Divider(
                thickness: 1,
              ),
            ),
          ),
          const SizedBox(
            width: 20,
          ),
        ],
      ),
    );
  }

  Widget _backToLogin() {
    return InkWell(
      onTap: () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => const Login()));
      },
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 2),
        padding: const EdgeInsets.all(3),
        alignment: Alignment.bottomCenter,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Sudah Mempunyai Akun?',
              style: mDesSubtitleStyle,
            ),
            const SizedBox(
              width: 10,
            ),
            Text('Masuk', style: mTitleStyle),
          ],
        ),
      ),
    );
  }

  Widget _title() {
    return SizedBox(
        height: 50,
        child: Image.asset(
          'assets/svg/AKAR.png',
        ));
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    return Scaffold(
        body: SizedBox(
      height: height,
      child: Stack(
        children: <Widget>[
          Positioned(
              top: -height * .15,
              right: -MediaQuery.of(context).size.width * .4,
              child: const BezierContainer()),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: SingleChildScrollView(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(height: height * .2),
                    _title(),
                    const SizedBox(height: 50),
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            'Nama',
                            style: mTitleStyle,
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          TextField(
                              controller: namaController,
                              decoration: const InputDecoration(
                                  border: InputBorder.none,
                                  fillColor: Color(0xfff3f3f4),
                                  filled: true))
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            'Username',
                            style: mTitleStyle,
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          TextField(
                              controller: usernameController,
                              decoration: const InputDecoration(
                                  border: InputBorder.none,
                                  fillColor: Color(0xfff3f3f4),
                                  filled: true))
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            'Password',
                            style: mTitleStyle,
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          TextField(
                              controller: passwordController,
                              obscureText: true,
                              decoration: const InputDecoration(
                                  border: InputBorder.none,
                                  fillColor: Color(0xfff3f3f4),
                                  filled: true))
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            'Sekolah',
                            style: mTitleStyle,
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          TextField(
                              controller: sekolahController,
                              decoration: const InputDecoration(
                                  border: InputBorder.none,
                                  fillColor: Color(0xfff3f3f4),
                                  filled: true))
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            'Kelas',
                            style: mTitleStyle,
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          TextField(
                              controller: kelasController,
                              decoration: const InputDecoration(
                                  border: InputBorder.none,
                                  fillColor: Color(0xfff3f3f4),
                                  filled: true))
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            'Alamat',
                            style: mTitleStyle,
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          TextField(
                              controller: alamatController,
                              keyboardType: TextInputType.multiline,
                              decoration: const InputDecoration(
                                  border: InputBorder.none,
                                  fillColor: Color(0xfff3f3f4),
                                  filled: true))
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 2),
                      padding: const EdgeInsets.all(3),
                      alignment: Alignment.centerLeft,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Text('Jenis Kelamin', style: mTitleStyle),
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 2),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: nList
                            .map((data) => RadioListTile(
                                  title: Text(data.title),
                                  groupValue: id,
                                  value: data.index,
                                  onChanged: (val) {
                                    setState(() {
                                      jenisKelamin = data.jk;
                                      id = data.index;
                                    });
                                  },
                                ))
                            .toList(),
                      ),
                    ),
                    const SizedBox(height: 20),
                    _submitButton(),
                    const SizedBox(height: 20),
                    _divider(),
                    SizedBox(height: height * .055),
                    _backToLogin(),
                    const SizedBox(height: 20),
                  ]),
            ),
          ),
        ],
      ),
    ));
  }

  void _signup() async {
    setState(() {
      _isLoading = true;
    });

    var data = {
      'name': namaController.text,
      'username': usernameController.text,
      'password': passwordController.text,
      'address': alamatController.text,
      'school': sekolahController.text,
      'class': kelasController.text,
      'sex': jenisKelamin
    };

    var res = await CallApi().postData(data, 'register');

    var body = json.decode(res.body);

    if (body['success']) {
      SharedPreferences localStorage = await SharedPreferences.getInstance();
      localStorage.setString('token', body['token']);
      localStorage.setString('user', json.encode(body['user']));

      Navigator.push(
          context, MaterialPageRoute(builder: (context) => const MyApp()));
    }

    setState(() {
      _isLoading = false;
    });
  }
}
