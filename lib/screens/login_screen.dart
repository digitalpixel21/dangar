import 'dart:convert';

// ignore: import_of_legacy_library_into_null_safe
import 'package:dangar/main.dart';
import 'package:dangar/network_utils/api.dart';
import 'package:dangar/screens/signup_screen.dart';
import 'package:flutter/material.dart';
import 'package:dangar/constants/style_constant.dart';
import 'package:dangar/widgets/bezierContainer.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:shared_preferences/shared_preferences.dart';

class Login extends StatefulWidget {
  final String title;
  // ignore: use_key_in_widget_constructors
  const Login({this.title = ""});

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  bool _isLoading = false;
  bool _validate = false;
  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  _showMsg(msg) {
    final snackBar = SnackBar(
      content: Text(msg),
      action: SnackBarAction(
        label: 'Close',
        onPressed: () {
          // Some code to undo the change!
        },
      ),
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  Widget _submitButton() {
    return SizedBox(
      height: 50.0,
      child: TextButton(
        onPressed: _isLoading ? null : _login,
        style: ButtonStyle(
          padding: MaterialStateProperty.all(const EdgeInsets.all(0.0)),
        ),
        child: Ink(
          decoration: BoxDecoration(
              gradient: const LinearGradient(
                colors: [Color(0xff117eeb), Color(0xffdb7285)],
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
              ),
              borderRadius: BorderRadius.circular(30.0)),
          child: Container(
            constraints: const BoxConstraints(maxWidth: 300.0, minHeight: 50.0),
            alignment: Alignment.center,
            child: Text(
              _isLoading ? 'Menunggu...' : 'Masuk',
              textAlign: TextAlign.center,
              style: const TextStyle(color: Colors.white),
            ),
          ),
        ),
      ),
    );
  }

  Widget _divider() {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 3),
      child: Row(
        children: <Widget>[
          const SizedBox(
            width: 20,
          ),
          const Expanded(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Divider(
                thickness: 1,
              ),
            ),
          ),
          Text(
            'atau',
            style: mServiceTitleStyle,
          ),
          const Expanded(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Divider(
                thickness: 1,
              ),
            ),
          ),
          const SizedBox(
            width: 20,
          ),
        ],
      ),
    );
  }

  Widget _createAccountLabel() {
    return InkWell(
      onTap: () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => const SignUp()));
      },
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 2),
        padding: const EdgeInsets.all(3),
        alignment: Alignment.bottomCenter,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Belum Mempunyai Akun ?',
              style: mDesSubtitleStyle,
            ),
            const SizedBox(
              width: 10,
            ),
            Text('Daftar', style: mTitleStyle),
          ],
        ),
      ),
    );
  }

  Widget _title() {
    return SizedBox(
        height: 80,
        child: Image.asset(
          'assets/svg/akar.png',
        ));
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    return Scaffold(
        body: SizedBox(
      height: height,
      child: Stack(
        children: <Widget>[
          Positioned(
              top: -height * .15,
              right: -MediaQuery.of(context).size.width * .4,
              child: const BezierContainer()),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(height: height * .2),
                  _title(),
                  const SizedBox(height: 50),

                  // username
                  TextField(
                      controller: usernameController,
                      onChanged: (value) {
                        if (value.isNotEmpty) {
                          setState(() {
                            _validate = false;
                          });
                        }
                      },
                      decoration: InputDecoration(
                          errorText: _validate ? 'Ada field kosong' : null,
                          hintText: "Username",
                          hintStyle: const TextStyle(
                              color: Color(0xFF9b9b9b),
                              fontSize: 14,
                              fontWeight: FontWeight.normal),
                          border: InputBorder.none,
                          fillColor: const Color(0xfff3f3f4),
                          filled: true)),

                  const SizedBox(height: 10),

                  // password
                  TextField(
                      controller: passwordController,
                      onChanged: (value) {
                        if (value.isNotEmpty) {
                          setState(() {
                            _validate = false;
                          });
                        }
                      },
                      obscureText: true,
                      decoration: InputDecoration(
                          errorText: _validate ? 'Ada field kosong' : null,
                          hintText: "Password",
                          hintStyle: const TextStyle(
                              color: Color(0xFF9b9b9b),
                              fontSize: 14,
                              fontWeight: FontWeight.normal),
                          border: InputBorder.none,
                          fillColor: const Color(0xfff3f3f4),
                          filled: true)),

                  const SizedBox(height: 20),

                  _submitButton(),
                  Container(
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    alignment: Alignment.centerRight,
                    child: Text('Lupa Password ?', style: mDesSubtitleStyle),
                  ),
                  _divider(),
                  SizedBox(height: height * .055),
                  _createAccountLabel(),
                ],
              ),
            ),
          ),
        ],
      ),
    ));
  }

  void _login() async {
    setState(() {
      _isLoading = true;
    });

    var data = {
      'username': usernameController.text,
      'password': passwordController.text
    };

    var res = await CallApi().postData(data, 'login');
    var body = json.decode(res.body);
    if (body['success']) {
      SharedPreferences localStorage = await SharedPreferences.getInstance();
      localStorage.setString('token', body['token']);
      localStorage.setString('pretest', body['pretest']);
      localStorage.setString('user', json.encode(body['user']));
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => const MyApp()));
    } else {
      _showMsg(body['message']);
    }

    setState(() {
      _isLoading = false;
    });
  }
}
