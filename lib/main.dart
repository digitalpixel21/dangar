// @dart=2.9

import 'package:dangar/constants/color_constant.dart';
import 'package:dangar/screens/pretest/pretest_screen.dart';
import 'package:dangar/widgets/bottom_tab_initial.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:dangar/screens/login_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:splashscreen/splashscreen.dart';

void main() => runApp(const MyApp());

class MyApp extends StatefulWidget {
  const MyApp({Key key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool _isLoggedIn = false;
  bool _isPreTest = false;

  @override
  void initState() {
    _checkIfLoggedIn();
    super.initState();
  }

  void _checkIfLoggedIn() async {
    // check if token is there
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var token = localStorage.getString('token');
    var pretest = localStorage.getString('pretest');

    if (token != null) {
      setState(() {
        _isLoggedIn = true;
      });
    }

    if (pretest == '0') {
      setState(() {
        _isPreTest = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: SplashScreen(
          seconds: 5,
          navigateAfterSeconds: _isLoggedIn
              ? (_isPreTest ? const PretestScreen() : const BottomTabInitial())
              : const Login(),
          title: const Text(
            'SplashScreen Example',
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20.0,
                color: Colors.white),
          ),
          image: Image.asset('assets/svg/akar.png'),
          photoSize: 120.0,
          backgroundColor: Colors.white,
          styleTextUnderTheLoader: const TextStyle(),
          loaderColor: kRedColor),
    );
  }
}
