class CarouselModel {
  final String image;

  CarouselModel({required this.image});
}

final List<Map<String, Object>> carousels = [
  {"image": "assets/images/carousel_flight_discount.png"},
  {"image": "assets/images/carousel_hotel_discount.png"},
  {"image": "assets/images/carousel_covid_discount.png"},
];
