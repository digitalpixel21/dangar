class Nilai {
  // ignore: non_constant_identifier_names
  int no = 0;
  // ignore: non_constant_identifier_names
  int nilai_id;
  // ignore: non_constant_identifier_names
  int user_id;
  String nilai;
  String tanggal;

  Nilai(this.nilai_id, this.user_id, this.nilai, this.tanggal);

  Nilai.fromMap(Map<String, dynamic> map)
      : nilai_id = map['nilai_id'],
        user_id = map['user_id'],
        nilai = map['nilai'],
        tanggal = map['tanggal'];

  Map<String, dynamic> toMap() {
    return {
      'no': no++,
      'nilai_id': nilai_id,
      'user_id': user_id,
      'nilai': nilai,
      'tanggal': tanggal,
    };
  }

  factory Nilai.fromJson(dynamic json) {
    return Nilai(json['nilai_id'] as int, json['user_id'] as int,
        json['nilai'] as String, json['tanggal'] as String);
  }
}
