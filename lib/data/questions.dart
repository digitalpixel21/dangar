import 'package:dangar/models/questions_model.dart';

List<QuestionModel> questions = [
  QuestionModel(
    "Salah satu cabang seni rupa dua dimensi dengan proses pembuatan karyanya memakai teknik cetak dan biasanya di atas kain mori dinamakan?",
    {
      "a.	Batik printing": true,
      "b.	Batik tulis": false,
      "c.	Sticker ": false,
      "d.	Lukisan": false,
      "e.	Gambar": false,
    },
  ),
  QuestionModel("Yang bukan contoh karya seni rupa dua dimensi yaitu?", {
    "a.	Relief": false,
    "b.	Seni arsitektur": true,
    "c.	Seni Lukis": false,
    "d.	Seni grafis": false,
    "e.	seni ilustrasi": false,
  }),
  QuestionModel(
      "Unsur visual dalam seni rupa 2 dimensi yang terbentuk karena hubungan beberapa garis dinamakan ?",
      {
        "a.	Titik ": false,
        "b.	Bangun": false,
        "c.	Bidang": true,
        "d.	Gelap Terang": false,
        "e.	Tekstur ": false,
      }),
  QuestionModel(
      "Kesan yang ditimbulkan oleh pantulan cahaya pada mata dinamakan?", {
    "a.	Bentuk": false,
    "b.	Tekstur": false,
    "c.	Gelap-terang": false,
    "d.	Warna": true,
    "e.	Ruang": false,
  }),
  QuestionModel(
      "Perbedaan intensitas cahaya yang jatuh pada permukaan benda menyebabkan munculnya tingkat nada warna yang memberi kesan … pada sebuah karya.",
      {
        "a.	Besar": false,
        "b.	Raut": false,
        "c.	Gelap terang": true,
        "d.	Warna ": false,
        "e.	Tekstur ": false,
      }),
  QuestionModel(
      "Campuran salah satu warna primer dengan salah satu warna sekunder menghasilkan warna?",
      {
        "a.	Komplementer": false,
        "b.	Analogus": false,
        "c.	Skunder": false,
        "d.	Tertier": false,
        "d.	Primer": true,
      }),
  QuestionModel(
      "Suatu karya seni harus memiliki unsur-unsur seni yang terkandung didalamnya. Unsur-unsur tersebut pada mulanya masih berdiri sendiri sehingga harus digabungkan untuk menciptakan karya seni yang menarik,  cara penggabungan unsur-unsur seni inilah yang kemudian disebut dengan ?",
      {
        "a.	Prinsip seni rupa": true,
        "b.	Hakikat seni rupa": false,
        "c.	Unsur seni rupa": false,
        "d.	Teknik seni rupa": false,
        "e.	Pengertian seni rupa": false,
      }),
  QuestionModel(
      "Kain motif kembang Dangar merupakan salah satu karya seni rupa 2 dimensi yang berasal dari daerah?",
      {
        "a.	Lombok utara": true,
        "b.	Lombok barat": false,
        "c.	Lombok tengah": false,
        "d.	Lombok timur": false,
        "e.	Sumbawa": false,
      }),
  QuestionModel(
      "Proses pembuatan kain motif kembang dangar menggunakan Teknik?", {
    "a.	Batik tulis": false,
    "b.	Batik printing": true,
    "c.	Tenun": false,
    "d.	Anyam": false,
    "e.	Tuang Berulang": false,
  }),
  QuestionModel("Salah satu bahan pembuatan kain motif Kembang Dangar yaitu?", {
    "a.	Malam": false,
    "b.	Kulit sintetis": false,
    "c.	Kain sutera": false,
    "d.	Kertas sublime": true,
    "e.	Cat akrilik": false,
  }),
];
